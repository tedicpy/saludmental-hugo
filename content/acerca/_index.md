---
title: "Acerca de"
description: "This is meta description."
author_image : ""
draft: false
---

La pandemia ha significado un cambio radical y brusco en nuestras vidas; cambios en la convivencia cotidiana, en los modos de trabajar y estudiar y sobre todo en el modo de socializar. Ante la necesidad de mantener la distancia social, nos vimos expuestos a buscar otros medios de conexión con el otro; medios que ya estaban, pero que este contexto de aislamiento parecen ser el modo más seguro de relacionarse. Esto implica pasar más tiempo encerrados de lo que elegimos pasar, y de igual manera, esto ocurre con el tiempo que pasamos conectados a los espacios digitales.

Debido a que el uso excesivo de tecnologías está relacionado al aumento de comportamientos y emociones relacionadas a la ansiedad, depresión, estrés y otros riesgos psicosociales, es importante contar con prácticas que contribuyan a un uso ágil y reflexivo de tecnologías que beneficien a nuestra salud mental, parte importante de nuestro bienestar integral.

Por ello, creamos el sitio de <a href="https://www.menteenlinea.org" target=" blank">**Mente en Línea**</a>, la campaña sobre Salud Mental en Internet, en donde te compartimos recursos sobre el tema en formato de episodios de podcast, fanzines ilustrativas que se pueden obtener tanto de manera digital o impresa, artículos e investigaciones para profundizar más sobre los distintos factores que impactan en nuestro comportamiento y salud mental a la hora de interactuar con tecnologías.

<a href="https://www.menteenlinea.org" target=" blank">**Mente en Línea**</a> es una campaña organizada por <a href="https://www.tedic.org/" target=" blank">TEDIC</a> con el apoyo de la <a href="https://www.gov.uk/world/paraguay">Embajada Británica en Asunción</a>, y en alianza con la <a href="https://www.mspbs.gov.py/salud-mental.html" target=" blank">Dirección de Salud Mental</a> del <a href="https://www.mspbs.gov.py/index.php" target=" blank">Ministerio de Salud Pública y Bienestar Social</a>.
