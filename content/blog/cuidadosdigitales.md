---
title: "Salud Mental y Prácticas de Cuidados Digitales en tiempos de COVID19"
date: 2019-05-12T12:14:34+06:00
image: "images/blog/cuidadosdigitales.png"
description: "Cuidados digitales y salud mental"
draft: false
---

En este post, queremos compartir contigo unos tips y herramientas que podrían ser de ayuda a la hora de interactuar con la tecnología, entendiendo la relación de las mismas con nuestro bienestar psicoemocional, nuestros procesos cognitivos, y la importancia de darnos una pausa en estos tiempos de incertidumbre global.

Mucho cambió en estos últimos meses, y de manera muy abrupta, ya que pasamos mucho más tiempo en nuestras cuatro paredes de lo que normalmente elegiríamos pasar. Aquellas personas con niñes, de repente tienen la tarea añadida de ver qué hacer con elles, cuidarles y ayudarles a pasar estos tiempos y lo que esta transición conlleva. Algunas personas estamos trabajando desde nuestras casas, incluso con una carga horaria más pesada, y otras nos encontramos afrontando dificultades para tener ingresos fijos o estables en estos tiempos en los cuáles la aglutinación de personas no está permitido.

Estas nuevas condiciones requieren nuevos comportamientos y hábitos, mientras nuestras mentes se encuentran en una búsqueda constante de soluciones a los distintos desafíos delante nuestro. Ante la situación de incertidumbre, pérdida de rutina y también de oportunidades, nuestro bienestar psicoemocional se puede ver afectado considerablemente. Estamos en un estado de constante recalibración y ajuste, muchas veces sin tener un norte, y a manera de salir mentalmente de nuestra situación de auto-encierro y preocupación, nos encontramos cada vez más inmersos en espacios digitales, buscando mantener nuestros vínculos y también maneras de acceder a información, expresarnos y mantener una sensación de cercanía a pesar de la distancia.


### Preguntas para conectar con nuestra cabeza que pelea o vuela

Estas condiciones de cuarentena y distanciamiento social para poder enfrentar esta situación de pandemia y de crisis requieren nuevos comportamientos y hábitos, lo cual también pone a nuestras mentes en un modo de lucha y búsqueda constante de soluciones a los distintos desafíos delante nuestro, siendo algunos:

* El manejo de preocupaciones sobre nosotres, personas a nuestro alrededor, y el futuro;
* Lidiar niveles de ansiedad y estrés mucho más intensos;
* Tratar de encontrar una rutina diaria;
* Reconciliar la carga de trabajo y de cuidado de la familia en las casas;
* Mantener contacto con otres;
* Tratar de estar al día con las obligaciones y ritmos de trabajo virtual, así como también acostumbrarse al uso de ciertas herramientas por primera vez.

Ante estos y muchos otros desafíos combinados con un panorama incierto, es muy fácil que nos encontremos en un panorama de “fight or flight” (pelea o vuela), buscando pelear constantemente o huir de las situaciones que nos preocupan y abruman. Esta sensación de alerta, ansiedad y estrés es beneficiosa en algunos casos, ya que nos mantiene alerta en situaciones clave, como por ejemplo, tenemos que rendir un examen, entregar un informe, o producto, lavarnos las manos y usar tapabocas al salir, entre otros. De igual manera, esta sensación puede ser dañina si ese estado de alerta es uno constante, del cual no nos podemos desconectar, y el cual nos lleva a un círculo vicioso en el cual pensamos constantemente en ciertos problemas o desafíos sin encaminar esfuerzos hacia encontrar una solución, llevando a un estado de desesperación e incluso desesperanza, vinculado con la depresión.

Nuestro rendimiento cognitivo se ve impactado por lo emocional, llevándonos a distorsionar ciertas situaciones, lo cual contribuye a un ciclo de ansiedad y estrés. Específicamente nuestra <a href="https://psicologiaymente.com/neurociencias/corteza-prefrontal" target=" blank">corteza pre-frontal</a> en nuestro cerebro, la cual maneja procesos cognitivos complejos como la memoria, el razonamiento, resolución de problemas y planeamiento, y la amígdala, parte de nuestro <a href="https://psicologiaymente.com/neurociencias/sistema-limbico-cerebro" target=" blank">sistema límbico</a> que regula nuestras emociones, se ven afectadas, creando una red de sobre-preocupación y miedo que puede influenciar nuestro pensar y sentir y se puede magnificar a la hora de habitar espacios digitales en los cuales nos encontramos comparando, asumiendo, y especulando muchas veces que otras personas se encuentran en mejor situación que la nuestra.

En estos casos, desde el área de Psicología Cognitiva se identificaron unas dinámicas de pensamiento que obstaculizan una visión objetiva y útil de la situación, conocidas como <a href="https://psychcentral.com/lib/15-common-cognitive-distortions/" target=" blank">distorsiones cognitivas</a>. Estas dinámicas fueron propuestas por el psicólogo Aaron Beck en 1976 y popularizadas por el psicólogo David Burns en 1980. Aquí te compartimos algunas:

* **Minimización y descalificación de lo positivo:** ¿Considerás que tus características o experiencias positivas son reales pero insignificantes? ¿Solés descontar experiencias positivas que te ocurren porque conflictúan con con tu visión negativa de ciertas cosas?
* **Razonamiento emocional:** ¿Asumís que tus reacciones emocionales reflejan realmente la situación?
* **Lectura de mentes:** ¿Asumís que otres reaccionan negativamente a tu persona sin evidencia de que así sea el caso?
* **Catastrofizando:** ¿Ves eventos negativos que podrían ocurrir como catástofres intolerables en vez de verlas con otra perspectiva?
* **Pensamiento polarizado:** ¿Ves una situación como algo muy bueno o algo muy negativo, sin un punto gris en el medio?
* **Abstracción selectiva:** Ante una situación compleja, ¿te concentrás solamente en un solo aspecto, ignorando los otros aspectos relevantes a tener en cuenta?
* **Personalización:** ¿Asumís que sos la causa de algún evento externo particular, cuando en realidad otros factores son los responsables?

Hacernos estas preguntas nos ayudan a poder identificar lo que sucede, analizar el tipo de distorsión, y re-interpretar la situación en base al cuestionamiento y la reformulación del pensamiento negativo por otro que sea más realista, cuantas veces sea necesario. Es una práctica que nos ayuda a poder tener un entendimiento de nuestro funcionamiento cognitivo en estos tiempos de crisis y también en tiempos de cambio, presión, conflicto e incertidumbre.

### Trabajemos en torno al cuidado

Ahora mismo, estas dinámicas de distanciamiento social y quedarnos en nuestras casas se hacen en base a promover el cuidado, de cierta manera. Pero a la par de ciertas exigencias que se dan ahora que pasamos a realizar intercambios en línea con mucha más frecuencia, sumando los desafíos que se presentan y que mencionamos más arriba, necesitamos también pensar en el cuidado de nuestro pensar y sentir: en nuestro bienestar social, psicológico y emocional no solamente a nivel individual sino también a nivel colectivo.

En estos tiempos en los cuales nuestras interacciones en línea fueron en aumento, ¿cómo nos cuidamos al habitar estos espacios? ¿Cómo seguimos manteniendo interacciones, vínculos y maneras de comunicarnos que sean humanas? Las compañeras del <a href="https://fondoaccionurgente.org.co/" target=" blank">Fondo de Acción Urgente de América Latina y el Caribe (FAU-AL)</a>, cuyo eje de trabajo tiene al cuidado desde el centro, cuentan con diversas prácticas y consejos a tener en cuenta para el <a href="https://fondoaccionurgente.org.co/site/assets/files/5765/1esp_tips_de_cuidado_digital.pdf" target=" blank">cuidado de nuestros cuerpos digitales</a> y nuestro <a href="https://fondoaccionurgente.org.co/site/assets/files/5747/tips_para_el_cuidado_emocional_2020.pdf" target=" blank">bienestar emocional</a> en estos momentos de crisis en base a sus experiencias como equipo de trabajo remoto. Aquí te las compartimos:

* **Es importante reconocer que tenemos diferentes niveles de uso, y experiencias con las tecnologías.** En base a esas diferencias, se deben establecer acuerdos sobre canales de comunicación a utilizar que propicien un buen nivel de interacciones con el mínimo nivel de estrés posible.

* **Cultivemos la comunicación asertiva:** Las compañeras del FAU-AL nos recomiendan transmitir las cosas de manera oportuna y directa para evitar acumular tensiones y preocupaciones. Ante situaciones de urgencia, consideremos realizar llamadas directas o videollamadas cuando sea necesario para construir espacios seguros de comunicación en donde no hayan monólogos virtuales a través de mensajes o audios, sino un espacio de diálogo y escucha activa que lleve a una resolución y alivie ansiedades o incertidumbres.

* **UBUNTU:** Ubuntu viene de las lenguas de la región sudafricana, que puede se traducida a “soy porque somos”. Este concepto se utiliza para hablar del sentido de unión, cooperación y solidaridad entre personas, contruyendo enlaces de confianza. Esto se traslada a lo digital en la capacidad de confiar en las habilidades e intenciones de las demás personas a la hora de trabajar en conjunto. Las compañeras hablan de la importancia de poder asumir ciertas lecturas con la mejor de las intenciones, entendiendo que al interactuar desde la distancia y a través de mensajes de texto o correos, por ejemplo, muchas veces se trasmiten cosas sin las emociones o tonos adjuntos. Por ende, la confianza y asumir las mejores intenciones se vuelve fundamental para no caer en lo que mencionamos más arriba, las distorsiones cognitivas que nos generan ansiedad y estrés.

* **Pausemos:** Tomemos una pausa activa, alejémonos de nuestras computadoras y dispositivos, démonos el tiempo de respirar largo y profundo, mover el cuerpo, estirar un poco, y dar un descanso a nuestros ojos. Sin tomarnos una pausa, no nos damos el tiempo para recalibrar energías, y tanto nuestro rendimiento como nuestro pensar y sentir se ven definitivamente impactados.

* **Seamos conscientes de nuestros privilegios digitales:** Esto es algo que desde TEDIC también lo reconocemos, ya que si bien trabajamos desde el marco de uso de software libre y herramientas seguras, sabemos que muchas veces las mismas no son accesibles para todas las personas, ya que nuestros niveles de autonomía y posibilidad de escoger varían de acuerdo a nuestros contextos. Dimensionemos estas diferencias, y con empatía apoyemos a las personas para hacer uso de herramientas tecnológicas de manera segura y ágil. Es un proceso que requiere paciencia y sorodidad 🙂

* **Un llamado a la calma:** Es una posibilidad de que el Internet nos abandone, que la conexión esté sobrecargada y eso la haga más lenta, y que por ende ciertas tareas o acciones tomen más tiempo. Las chicas nos comentan sobre la importancia de tomarnos un respiro y entender que ciertas cosas están fuera de nuestro control, y que no debemos frustrarnos por algo que en realidad se escapan de nuestra responsabilidad. Intentemos afrontar estas dificultades con paciencia y también con compasión hacia nosotres.

* **Busquemos el equilibrio:** Es importante escuchar el ritmo de cada persona, buscando el equilibrio entre tiempos institucionales, personales y colectivos. ¿Te diste cuenta de que tenés un mejor rendimiento a la tarde o noche que a la mañana? Las compañeras recomiendan poder establecer estos tiempos en base a los ritmos de cada persona, con tal de que se acuerden tiempos en que todes puedan coincidir para tener reuniones en equipo o resolver cuestiones que requieren un diálogo y planificación más directa. Esto ayuda a adquirir un balance y un rendimiento más efectivo, en base a entender de que cada persona tiene sus ritmos.

* **Corresponsabilidad digital:** El cuidado en entornos digitales no solamente conlleva prácticas individuales de cuidado, sino también prácticas y hábitos que tengan en cuenta el cuidado y la seguridad de las demás personas. Es importante entender hasta dónde llega la información que comparto o a las cuales tienen acceso aplicaciones y herramientas que utilizo, y cuánto se puede saber de mis contactos a través de esas vías y huellas digitales mías.

* **Compartir de manera segura:** Es necesario reflexionar y dialogar en equipo sobre la información y datos que compartimos a través de los canales y plataformas que utilizamos. ¿Qué tan seguras son? ¿Qué ocurriría si terceros tienen acceso? ¿Tenemos algún protocolo de acción y cuidado que nos sirva para mitigar estos riesgos?

¿Dónde podés empezar? Acá te compartimos nuestros posteos sobre los siguientes temas en seguidad digital que te podrían ser de utilidad:

* <a href="https://www.tedic.org/el-caos-de-las-contrasenas/" target=" blank">Contraseñas seguras</a>
* <a href="https://www.tedic.org/respaldo-de-datos-protegiendo-lo-mas-importante-de-tu-organizacion/" target=" blank">Respaldo de datos</a>
* <a href="https://www.tedic.org/la-proteccion-digital-en-tu-organizacion/" target=" blank">Seguridad digital de la organización</a>
* <a href="https://www.tedic.org/como-proteger-la-privacidad-de-tu-informacion/" target=" blank">Cifrado y privacidad de la organización</a>
* <a href="https://www.tedic.org/videoconferencias-seguras-y-para-todes/" target=" blank">Videollamadas seguras</a>
* <a href="https://www.tedic.org/por-que-tener-habitos-de-higiene-en-el-entorno-digital/" target=" blank">Higiene Digital</a>
* <a href="https://www.tedic.org/toolkit-internews/" target=" blank">Seguridad digital para acciones colectivas</a>
* y mucha más info sobre seguridad digital que podés encontrar <a href="https://www.tedic.org/tag/seguridad-digital/" target=" blank">acá</a>.

* **Tomemos en serio nuestro almuerzo y procuremos no comer en frente a la computadora o nuestro celular.** Aprovechemos este momento para tomar esa pausa activa y recalibrar energías y emociones.

* **Abracemos la incertidumbre con esperanza:** Las compañeras del FAUL-AL nos recuerdan que la vida no la vivimos a través de nuestras pantallas, sino más bien a través de nuestro cuerpo y piel. Estamos ahora en falta de oxitocina por no abrazarnos y tener el mismo nivel de contacto con los demás como antes. Pensemos cuándo nuevamente podremos retomar ciertos hábitos de contacto físico (teniendo todos los cuidados necesarios) con esperanza, con emoción y energías positivas.

* **Que la bola de nieve no nos atropelle:** Tratemos de no sobrecargarnos de información y démonos la chance de desconectarnos, y desintoxicarnos de las redes sociales, de ser necesario. Habitemos estos espacios de manera más conciente.

¿Y cómo hacerlo? De eso les hablaremos ahora 🙂

### Tips de desintoxicación digital para resistir

Es una realidad que el uso de dispositivos y estar frente a la pantalla nos desgasta. Realizar ciertas actividades, como reuniones vía videollamadas o tener que estar constantemente alerta a cualquier mensaje o correo que recibamos mientras de paso nos bombardean de noticias e incluso desinformación, también tiene un impacto. Realizamos un esfuerzo cognitivo muy grande, que en definitiva agota nuestros niveles de energía, haciendo un ping pong entre revisar nuestras redes, ver toda una temporada de una serie en una sola noche, o pasar de una app a otra, diluyendo nuestra atención y energía.

Cada tanto es importante hacer una desconexión y desintoxicación del uso de nuestros dispositivos, ya que estar en un estado de constante conexión no solamente tiene un impacto negativo en lo cognitivo, sino también físico.

¿Alguna vez experimentaste dificultad de conciliar el sueño luego de utilizar tu computadora, tablet o celular poco antes de ir a la cama, o en la cama misma? Esta ocurrencia es conocida <a href="http://www.cyborganthropology.com/Junk_Sleep" target=" blank">Junk Sleep</a>, o “Sueño basura”. El término se refiere al efecto fisiológico en donde el estado de sueño profundo no se logra o se ve interrumpido debido al uso de dispositivos electrónicos poco antes de haber conciliado el sueño (añadiendo el sonido o vibración de notificaciones, lo cual rompe el sueño constantentemente).

El brillo de las pantallas, la portabilidad de los dispositivos, y la naturaleza y tipo del contenido que consumimos juegan un rol en conectar nuestro cerebro a ciertos flujos de actividad que nos alejan del estado de sueño. Por ejemplo, utilizar una red social o navegar en Internet antes de dormir manipula al cerebro para que se vuelva más activo, en vez de preparlo para dormir, creando un estado falso e innecesario de alerta. Se recomienda no utilizar los dispositivos como mínimo de media hora antes de ir a dormir, para tener un sueño profundo que recargue energías.


Además, es importante saber que las plataformas de redes sociales están hechas para tratar de tenernos conectades la mayor cantidad de tiempo posible, utilizando mecánicas de diseño persuasivo para que liberemos dopamina, nuestra hormona anticipatoria del placer, que se libera cuando sentimos que recibimos una recompensa al recibir ese like, ese comentario, o esa reacción que nos hace sentir bien y de cierta manera validadas.

Si querés saber más sobre el rol de la dopamina al interactuar con nuestros dispositivos, podés leer nuestro artículo sobre <a href="https://www.tedic.org/tecnologia-persuasiva-dinamicas-y-patrones-de-enganche-digital/" target=" blank">tecnología persuasiva y patrones de enganche digital</a>, en donde hablamos de cómo las tecnologías están justamente hechas para tenernos conectades la mayor cantidad de tiempo posible y estimularla.

Si preferís informarte al respecto de otra manera, también tenemos un <a href="https://www.mixcloud.com/cyborgradio/enganchados-digitales/" target=" blank">episodio de podcast</a> al respecto.

### Ante todo, el cuidado en estos tiempos

Es importante entender que en estos tiempos nos rodea mucha inseguridad, incertidumbre y cambios que nos afectan de distintas maneras. Esperamos que estos tips les sean de utilidad para avanzar hacia la creación y/o adquisición de prácticas de cuidado de nuestra salud mental en estos tiempos, y de que entendamos que sentir preocupación, ansiedad y estrés en estos tiempos es normal. Aprendamos a ser flexibles con nuestras rutinas y a ejercer la compasión hacia nosotres, entendiendo la particularidad de estos tiempos.

¿Te identificás con lo mencionado y querés saber cómo lidiar con estas emociones? Aquí también te compartimos una <a href="https://www.psychologytools.com/assets/covid-19/guide_to_living_with_worry_and_anxiety_amidst_global_uncertainty_es.pdf" target=" blank">excelente guía</a> hecha por <a href="https://www.psychologytools.com" target=" blank">Psychologytools</a> para vivir y lidiar con preocupación y ansiedad en estos tiempos de ansiedad global.

Si te sentís o encontrás:

* Pensando sobre ciertos temas o problemas de manera repetida, sin llegar a una solución;
* Teniendo problemas conciliando el sueño;
* Pérdida de apetito o apetito en exceso;
* Dificultad concentrándote y tomando decisiones;
* Abrumándote o irritándote con facilidad

Y estas cosas te afectan considerablemente, te recomendamos que consultes con un profesional. Hay canales de comunicación preparados para ayudarte en estos tiempos. Acá te compartimos un canal de Paraguay del equipo de Salud Mental y Educación del Comité Científico del CONACYT (Consejo nacional de ciencias y tecnología) para la contingencia contra el COVID-19 que ofrece servicios de salud mental y acompañamiento psicológico en estos tiempos: https://twitter.com/SaludMental_Py

También, si sentís que vos o alguien que conozcas corre el riesgo de provocarse daño autoinfligido o de considerar el suicidio en base al contenido que publica en las redes, te compartimos los centros de ayuda que tienen <a href="https://help.twitter.com/es/safety-and-security/self-harm-and-suicide" target=" blank">Twitter</a>, <a href="https://www.facebook.com/help/1553737468262661" target=" blank">Facebook</a> e <a href="https://help.instagram.com/252214974954612" target=" blank">Instagram</a> en relación a este tipo de comportamientos, incluyendo trastornos alimenticios.

Para más información, también te recomendamos que sigas el hashtag de #HablemosDeSaludMentalEnCuarentena y #TuSaludMentalTambién, así como el hashtag de #MenteEnLínea en las redes sociales.

> _Posteo original en el <a href="https://www.tedic.org/salud-mental-covid19/" target=" blank">blog</a> de <a href="https://www.tedic.org" target=" blank">TEDIC</a>._
