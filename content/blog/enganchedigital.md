---
title: "Tecnología Persuasiva - Dinámicas y patrones de enganche digital"
date: 2019-05-12T12:14:34+06:00
image: "images/blog/enganchedigital.png"
description: "Conocé qué mecánicas de diseño y uso de tecnologías impactan en nuestro día a día"
draft: false
---

Actualmente, la cantidad de nuestro tiempo que va dirigido a estar conectados ha incrementado desde el mayor alcance y accesibilidad tanto de acceso a Internet como de acceso a dispositivos. Por ende, es importante comprender el efecto que tiene el uso constante de dispositivos en nuestro comportamiento y bienestar integral, el cual se refleja tanto en el mundo físico como en el digital.

Tomamos en cuenta a ramas de estudio como la <a href="http://cyborganthropology.com/Main_Page" target=" blank" >Cyborg Antropología</a> que exploran el concepto de los dispositivos como una extensión de nosotros mismos, ya que a pesar de no ser directamente parte de nuestros cuerpos, representan una parte de nuestra identidad y un medio por el cual nos manifestamos y expresamos, así como también a áreas como <a href="https://www.interaction-design.org/literature/topics/human-computer-interaction" target=" blank" >Interacción Humana con la Computadora</a> y <a href="https://www.sciencedirect.com/book/9781558606432/persuasive-technology" target=" blank">Tecnología Persuasiva</a>. Estas áreas, aparte de facilitar la interacción entre usuario y distintos tipos de dispositivos y plataformas, también estudian las diferentes maneras en las cuáles pueden lograr agarrar la atención y el tiempo del usuario la mayor cantidad de tiempo posible, en la mayor cantidad de situaciones posibles.

Muchos tendemos a mirar nuestro teléfono cuando estamos aburridos o sin nada que hacer, o incluso interrumpimos tareas para revisar si recibimos alguna notificación o si hay alguna novedad en las redes, y no necesariamente lo hacemos cuando estamos solos, ya que también lo hacemos cuando estamos en plena compañía. Y estas dinámicas de uso, que ya en su mayoría ocurren por inercia o acostumbramiento, no ocurren solamente por casualidad; existen factores importantes a la hora de entender el por qué estamos conectados todo el tiempo, y en este post nos gustaría mencionar alguno de ellos a manera de crear una base de debate y cuestionamiento.

### La re-programación de nuestro sistema dopaminérgico

Interactuar constantemente con el feed de nuestras redes sociales. Anticipar likes en Instagram o en Facebook, una notificación, respuesta o comentario a algo que compartimos. Estos comportamientos son ejemplos de cómo nos encontramos en un estado de constante alerta y conectividad buscando o esperando que ocurra algo, y va de mano con el aumento de la dopamina, la cual es el neurotransmisor de la anticipación del placer, responsable de mediar o regular la motivación que sentimos para hacer algo. Y en estos comportamientos de uso, es la anticipación de recibir una recompensa lo que nos deja en un estado de alerta constante, ya que recibir algo, o incluso anticiparlo, se siente bien.

Compañías de tecnología y las áreas asociadas al diseño y uso de dispositivos y plataformas han logrado entrar a nuestro sistema dopaminérgico, y de cierta manera re-programarlo gracias al refuerzo de ciertas conductas a través de la promesa de obtener una recompensa, Por ende, lo realizado por estos grupos consiste en explotar este mecanismo de recompensa para asegurar el mayor tiempo de uso de interacción posible. Ya la competencia no se vuelve otra plataforma o dispositivo similar. Ya la competencia se volvió el tiempo que dedicamos a nosotros mismos fuera del uso de dispositivos, y fuera de la necesidad de ver qué ocurre en estos espacios digitales de manera constante.

### El tragamonedas del scrolling

El diseñador ético y fundador de organizaciones como Time Well Spent (Tiempo Bien Gastado)y de <a href="https://www.humanetech.com/" target=" blank"> The Center for Humane Technology</a> (el Centro para Tecnología Humana) Tristan Harris expone que el diseño de ciertas plataformas digitales adoptaron mecánicas similares a la de los casinos, con el objetivo de establecer escenarios que propicien la estimulación de los sistemas de recompensa de los usuarios. Con el scrolling, se ven mecánicas similares a las de las máquinas tragamonedas, ya que ambos instalan la promesa de obtener una recompensa variable y enganchan a los usuarios con el constante impulso de ver con qué se van a encontrar, o qué van a obtener como el resultado del tiempo y/o recursos invertidos en la interacción. La función del scroll-down permite que una página se actualice infinitamente, y también ya existen funciones que permiten a los usuarios hacer un clic para que la página lo haga, mecánica que imita de cierta manera jalar la palanca de un tragamonedas para ver qué recompensa reciben, y lo que engancha a los usuarios es el instante de participación y anticipación de esa interacción.

<a href="https://hipertextual.com/2018/02/tristan-harris-filosofo-google-que-quiere-liberarnos-nuestro-smartphone" target=" blank">De acuerdo a Harris</a>, lo adictivo radica en la sorpresa e impredictabilidad, e indica que previamente a la interacción con estas plataformas digitales, no sabemos si nos encontraremos con un montón de likes, si recibiremos un correo, meme o imagen interesante, un anuncio, o nada.. Y en eso radica el aumento de dopamina. Aunque el placer encontrado en realizar estas interacciones no sea duradera, la posibilidad y disponibilidad de una recompensa de búsqueda resulta adictiva de por sí, llevando como resultado a un comportamiento de enganche y de conectividad constante, haciendo que periódicamente los usuarios interactúen con ciertas plataformas y estimular su sistema de recompensa con dosis intermitentes.

### Una aplicación para hacer nada

Existe una aplicación que juega con diferentes mecánicas de uso para demostrar cómo nuestro sistema dopaminérgico está enganchado con las mismas, independientemente de si recibimos contenido que nos agrada o esté relacionado a nuestro perfil. La aplicación se llama <a href="https://www.theatlantic.com/technology/archive/2017/06/the-app-that-does-nothing/529764/">Binky</a> (la versión en inglés de pacificador o chupete), y fue creada por Dan Kurtz, quien tuvo un similar interés en plasmar que la adicción radica en la anticipación y en la impredictabilidad, agregando el componente de que lo que importa más es lograr la conectividad y el enganche constante de los usuarios, que el contenido visualizado en sí.

<center><img src="/images/blog/Binky.jpeg" width="25%;"></center>

Esta aplicación simula una red social con todas las mecánicas adictivas: tapping, scrolling, dar likes (binking en la app), incluso las de swipe left y swipe right, utilizadas en aplicaciones de citas. Esta “red social” no contiene redes ni instancias para socializar. Es simplemente un ecosistema construido aleatoriamente para realizar acciones que no llevan a nada, y por ende no requieren de ningún esfuerzo cognitivo. La interface simula una modalidad de Instagram, en la cual una diversa cantidad de imágenes pueden ser visualizadas. Estas imágenes tienen etiquetas, aparecen aleatoriamente y son generadas de manera ilimitada. Dar un like a una imagen no hace nada. Swiping o dar un súper like o re-binking a la imagen tampoco. Incluso la opción de comentar te lleva a los usuarios a ingresar un texto aleatorio, fuera del control y manipulación de los usuarios.

La creación de esta aplicación refleja una situación recurrente en la mayoría de los usuarios: ¿Te pasó alguna vez que entraste a alguna plataforma digital, hiciste scrolling, swipe left, swipe right, le diste like a algo, comentaste en algo, pero luego no lo recordás, incluso segundos después de haber dejado de interactuar con el dispositivo? El objetivo de Binky es también exponer y crear debate en cuanto al estado actual de los usuarios, que ya por defecto entraban a ciertas plataformas a través de dispositivos constantemente, pero que ya no realizaban ningún esfuerzo cognitivo a la hora de recordar lo visto o encontrado.

### Mejorando nuestros hábitos de uso

Aparte de la recomendación de Harris de pensar si lo que estamos realizando es realmente útil para nosotros cada vez que interactuamos con nuestros dispositivos, también es recomendable adquirir prácticas para mejorar hábitos de uso y así efectivizar nuestro tiempo y atención. Acá te compartimos unos <a href="https://www.humanetech.com/take-control">tips</a> sugeridos por Humane Tech para tomar control de nuestro tiempo y hábitos de interacción:

#### Apagá todas las notificaciones, exceptuando las de personas

No es por casualidad que las notificaciones aparecen en rojo. El color está utilizado estratégicamente porque atrae nuestra atención de manera instantánea. Actualmente, muchas notificaciones son generadas por máquinas, y no por nuestros contactos u otras personas. Esto es con la intención de dirigir nuestra atención a aplicaciones o plataformas para persuadirnos de interactuar con ellas a pesar de no necesitar hacerlo realmente en el momento.

Ingresá a Configuración > Notificaciones y apagá toda notificación de aplicaciones que no consideres pertinente, exceptuando aquellas en donde gente real necesita de tu atención, como el caso de aplicaciones de mensajería como WhatsApp, Signal, Telegram, FB Messenger y otros. Dentro de cada aplicación, silencia las notificaciones de grupos de los que sos parte, pero en los cuales poner tu atención no es una prioridad.

#### Colocá un filtro de Blanco y Negro a tus dispositivos

<img src="/images/blog/blancoynegro.gif" class="responsive">


###### Gif de Artículo de <a href="https://www.nytimes.com/2018/01/12/technology/grayscale-phone.html">The New York Times</a>

Los íconos y logos de las aplicaciones también son coloridos y llamativos, con el propósito de llamar nuestra atención. Nuestro sistema de recompensa es altamente adictivo a la acción de interactuar con estos colores, por ende tendemos a pasar mucho más tiempo de lo necesario mirando imágenes y/o diferentes estímulos coloridos al interactuar con nuestros dispositivos. Una alternativa es colocar un filtro de colores en tu dispositivo, lo cual es recomendado para reducir el tiempo de interacción con otras aplicaciones, y chequear nuestros teléfonos con menos frecuencia.

Para colocar este filtro existe más de una opción. De acuerdo al sistema operativo de cada dispositivo (IOS o Android) podés realizar lo siguiente:

* Para **IOS (iPhones):** Ingresá a **Configuración** < **General** < **Accesibilidad** < **Adaptaciones de pantalla** < **Filtros de colores** < Activar la opción para tener acceso a la **Escala de grises**.

<div class="row w-100 justify-content-center">
  <div class="col-sm-12  col-md-4 gallery">
    <img src="/images/blog/iphone1.jpeg">
  </div>
  <div class="col-sm-12 col-md-4 gallery">
    <img src="/images/blog/iphone2.jpeg">
  </div>

  <div class="col-sm-12 col-md-4 gallery">
    <img src="/images/blog/iphone3.jpeg">
  </div>
  <div class="col-sm-12 col-md-4 gallery">
    <img src="/images/blog/iphone4.jpeg" >
  </div>
</div>

* Para **Android:** Ingresá a **Configuración** < **Opciones del Programador** (la cual al utilizar por primera vez se activa al hacer tapping de 3 a 5 veces) < **Simular espacio de color** < **Acromatopsia**.


<div class="row w-100 justify-content-center">
  <div class="col-sm-12  col-md-3 gallery">
  <img src="/images/blog/android1.jpeg" class="imagen">
  </div>
  <div class="col-sm-12 col-md-3 gallery">
  <img src="/images/blog/android2.jpeg" class="imagen">
  </div>
  <div class="col-sm-12 col-md-3 gallery">
  <img src="/images/blog/android3.jpeg" class="responsive">
  </div>
</div>
<div>


#### Intentá tener solamente tus aplicaciones útiles en la pantalla principal

Para evitar ingresar a aplicaciones por inercia ya que están en la pantalla principal, podés limitar tanto esa pantalla como la primera página o sección de aplicaciones a solamente las herramientas o aplicaciones que utilizás para tareas concretas, como
Mapas, Cámara, Calendario, Notas y otros. Podés colocar el resto de las aplicaciones en una carpeta aparte.

#### Tipea en la barra de búsqueda para tener acceso a aplicaciones de ocio

  <img src="/images/blog/fb.jpeg" class="responsive">


Podés ir a la barra de búsqueda de tu menú de aplicaciones para tipear y encontrar la aplicación, a manera de tomarte el suficiente esfuerzo para encontrarla, y también preguntarte si de verdad desearías pasar tu tiempo utilizando la aplicación.

#### Cargá tus dispositivos fuera del dormitorio

Utilizá una alarma separada en tu dormitorio, y de ser posible, cargá tu teléfono fuera del mismo, o en todo caso, al otro lado del dormitorio. De esta manera, podés despertarte sin automáticamente quedarte usando el teléfono o dispositivo antes de salir siquiera de la cama.

#### Remové las aplicaciones de redes sociales de tu teléfono

Esta es una de las alternativas más difíciles, pero la más efectiva en casos de desear pasar menos tiempo en nuestros dispositivos. Una alternativa sería utilizar estas aplicaciones desde la computadora, o hacer un esfuerzo y tipearlas desde el navegador, lo que va de mano con el punto relacionado a tipear en la barra de búsqueda del dispositivo.

#### Enviá notas de audio o llamá envés de mensajear

De acuerdo a estudios realizados, que la gente malinterprete nuestros mensajes de texto es común, mientras un mensaje de voz demuestra una eficiencia mayor debido a la apreciación del tono para la interpretación del mensaje. Además, grabar mensajes de voz es muchas veces mucho más rápido y menos estresante que estar tipeando. También, no requiere de tu atención visual al 100%.

Para esta recomendación es importante adecuarse al contexto, y entender que muchas veces otros usuarios no se encuentran en un ambiente en donde puedan escuchar notas de audio, entonces puede que estas tarden en escucharse o recibir respuesta en comparación a mensajes. Adicionalmente, cada persona es distinta; algunos contactos prefieren de hecho, recibir mensajes de texto a tener que escuchar audio por audio, en especial en casos en los que los mensajes son breves.

#### Tratá de utilizar atajos a la hora de mensajear

Utilizar diccionarios muchas veces ayuda a minimizar el tiempo que gastamos tipeando. Esto es recomendado en casos en los que se prefiera realizar la comunicación por mensajes de texto, aunque la debilidad es los riesgos de malinterpretación y errores son mayores (errores de Autocorrect) y requiere 100% de nuestra atención.

#### Descargá aplicaciones o extensiones que te puedan ayudar a habitar Internet sin distracciones

The Center for Humane Technology realizó una recopilación de aplicaciones y extensiones que facilitan una interacción con dispositivos más útil y efectiva. En este repositorio se encuentra una lista de herramientas que regulan la luz de nuestras pantallas, ayudan a la concentración en prioridades, bloquea el uso de ciertos sitios momentáneamente, pone límites al uso de dispositivos por un periodo de tiempo determinado para tomarte un descanso, y calcula cuánto tiempo pasás utilizando ciertas aplicaciones.

### Nuestra invitación (y desafío)

En estos tiempos de hiperconectividad en donde nos pasamos, bloqueando y desbloqueando las pantallas de nuestros dispositivos, Te invitamos a que cuestiones ciertos patrones de uso a través de lo siguiente:

* Esa aplicación que utilizo cada tanto y en la que paso mucho tiempo ¿Qué mecánicas utiliza para enganchar mi atención?
* Apenas me despierto, ¿me quedo por un tiempo considerable en la cama mirando mi teléfono?
* ¿Cuál es el porcentaje de tiempo que dedico a revisar mis aplicaciones diariamente?
* ¿Qué porcentaje considero que abarca el tiempo innecesario que invierto, a comparación del tiempo en que utilizo mi dispositivo para realizar algo útil?

Además, ¡te desafiamos a que uses el filtro de **escala de grises** por un tiempo y nos comentes tu experiencia! Puede ser por una semana, o para los más valientes, incluso un mes o más.

Para saber más, podés también escuchar nuestro episodio de podcast <a href="https://www.mixcloud.com/cyborgradio/enganchados-digitales/" target=" blank">Enganchados Digitales</a>, en nuestra <a href="https://cyborgfeminista.tedic.org/radiocyborg/" target=" blank">Radio Cyborg</a>.

> _Posteo original en el <a href="https://www.tedic.org/tecnologia-persuasiva-dinamicas-y-patrones-de-enganche-digital/" target=" blank">blog</a> de <a href="https://www.tedic.org">TEDIC</a>._
