---
title: "White Paper: Salud Mental en Internet y uso de tecnologías"
date: 2019-05-12T12:14:34+06:00
image: "images/blog/Whitepaper_SaludMental.png"
description: "Salud Mental en Internet y uso de tecnologías"
draft: false
---

En miras a la actual construcción de la nueva Política Nacional de Salud Mental, es de suma necesidad contar con un entendimiento integral de cómo las tecnologías son diseñadas e impactan en nuestra salud mental. Esto permitirá avanzar hacia la construcción y establecimiento de protocolos que promuevan prácticas para un uso más ágil y reflexivo de las tecnologías, que apunten a potenciar, y no a limitar, el bienestar psicoemocional de las personas a nivel individual y colectivo.

El presente documento expone el impacto del uso de tecnologías e Internet en nuestra atención, relaciones interpersonales, rendimiento físico, desarrollo cognitivo y emocional. Se exponen también a las áreas científicas relacionadas al diseño y uso de tecnologías y cómo impactan en la salud mental de los usuarios como resultado del uso excesivo de las mismas.

<center><a href="/white_paper/Salud-Mental-en-Internet.pdf" target=" blank"><button class="btn-lg btn-primary border-0">Leer aquí</button></a></center>
