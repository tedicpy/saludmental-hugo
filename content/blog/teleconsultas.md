---
title: "White Paper: Teleconsulta en salud mental y el uso de nuevas tecnologías"
date: 2019-05-12T12:14:34+06:00
image: "images/blog/Whitepaper_Teleconsulta_en_salud_mental.png"
description: "Teleconsulta en salud mental y el uso de nuevas tecnologías"
draft: false
---

En Paraguay, los telecuidados o teleconsultas de salud mental se presentan como una innovadora solución a una serie de problemas actuales y de gran significancia en el país. Los telecuidados de salud mental podrían complementar la red de salud mental existente, ofreciendo una mayor conectividad y comunicación entre los profesionales de salud, y un mayor acceso a atención especializada de calidad, haciendo muchas veces innecesario el traslado de los usuarios a estos servicios. En este contexto, los telecuidados de salud mental se presentan como una herramienta que podría colaborar a disminuir las
brechas en pos de una distribución más accesible de los recursos sanitarios y la atención en salud mental.

<center><a href="/white_paper/Teleconsulta-en-salud-mental.pdf" target=" blank"><button class="btn-lg btn-primary border-0">Leer aquí</button></a></center>
