# Web menteenlinea

Archivos para el generador de sitios Hugo, para la web https://www.menteenlinea.org/ 

Si encuentras un error o quieres colaborar en algo, no dudes hacer un "merge request"

Por más información de este proyecto, accede aquí: https://www.tedic.org/proyecto/menteenlinea

## Cómo configurar y utilizar este repositorio

Pre-requisito: [instalar hugo](https://gohugo.io/)

(puede ser con Docker, ver más abajo)

1. Clonar este repositorio:
```
git clone git@gitlab.com:tedicpy/saludmental-hugo.git
```

2. Moverse a la carpeta
```
cd saludmental-hugo
```

3. Levantar servidor de desarrollo
```
hugo server
```

4. Una vez hechos los cambios, se cancela con CTRL+c y se compila:
```
hugo
```

Esto carga todo el contenido en la carpeta "public"

## Usar hugo con Docker

Con este método Docker descarga y ejecuta Hugo. 

Es necesario abrir una terminal y hacer cd a la carpeta del proyecto.

Como servidor:

Permite ver los cambios del código en tiempo real en locahlost:1313 

```
docker run --rm -it   -v $(pwd):/src   -p 1313:1313   klakegg/hugo:0.101.0   server
```

Compilación:

```
docker run --rm -it   -v $(pwd):/src   klakegg/hugo:0.101.0
```

Esto compila el proyecto y lo pone en ./public


